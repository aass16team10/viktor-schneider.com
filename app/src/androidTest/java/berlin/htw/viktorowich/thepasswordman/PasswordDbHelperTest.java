package berlin.htw.viktorowich.thepasswordman;

import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

/**
 * Created by viktorowich on 17/07/16.
 */
public class PasswordDbHelperTest extends AndroidTestCase {

    private PasswordDbHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        RenamingDelegatingContext context = new RenamingDelegatingContext(getContext(),"test_");
        context.deleteDatabase(PasswordDbHelper.DATABASE_NAME);
        dbHelper = new PasswordDbHelper(context);
        this.db = dbHelper.getReadableDatabase();
        //dbHelper.onCreate(db);
    }

    @Override
    protected void tearDown() throws Exception
    {
        dbHelper.close();
        super.tearDown();
    }


    public void testOnUpgrade() throws Exception
    {

    }


    public void testOnCreate() throws Exception
    {
        InputStream inputStream = getContext().getAssets().open("passwords.txt");
        boolean check = false;
        if(inputStream != null)
            check = true;
        //assertEquals(check,true);
        assertTrue(check);

    }
    public void test2OnCreate() throws Exception
    {
        Cursor mCursor = db.rawQuery("SELECT * FROM " + PasswordDbHelper.TABLE_NAME, null);
        Boolean rowExists;

        if(mCursor.getCount() == 0)
        {
            rowExists = false;
        }
        else
        {
            rowExists = true;
        }
        assertTrue(rowExists);

    }
}