package berlin.htw.viktorowich.thepasswordman;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;
import org.junit.Test;

import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Created by viktorowich on 17/07/16.
 */
public class DownloadTest extends AndroidTestCase{


    private Download download;
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.download = new Download();


    }

    @Test
    public void testLoadFile() throws Exception
    {
        String url = "http://download.viktor-schneider.com/7-more-passwords.txt";
        URL url1 = new URL(url);
        HttpURLConnection huc =  (HttpURLConnection)  url1.openConnection ();
        huc.setRequestMethod ("GET");  //OR  huc.setRequestMethod ("HEAD");
        huc.connect () ;
        int code = huc.getResponseCode() ;
        System.out.println(code);
        boolean check;
        if(code==200)
            check=true;
        else
            check= false;

        assertTrue(check);
    }
}