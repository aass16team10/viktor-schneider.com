package berlin.htw.viktorowich.thepasswordman;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import static org.junit.Assert.*;

import static org.mockito.Mockito.when;

/**
 * Created by viktorowich on 14/07/16.
 */
public class Tests
{

    @Test
    public void testDB()
    {
        PasswordDbHelper mock = Mockito.mock(PasswordDbHelper.class);
        when(mock.getDBVersion()).thenReturn(13);
        assertEquals(mock.getDBVersion(),13);
    }
    /*@Test
    public void testDB_2()
    {
        PasswordDbHelper mock = Mockito.mock(PasswordDbHelper.class);
        when(mock.getDBname()).thenReturn(PasswordDbHelper.getDBname());
        assertEquals(mock.getDBVersion(),PasswordDbHelper.getDBname());
    }
@Test
    public void testDB_3()
    {
        PasswordDbHelper mock = Mockito.mock(PasswordDbHelper.class);
        when(mock.getDBcreateStatistic()).thenReturn(PasswordDbHelper.TABLE_NAME_STATISTIC);
        assertEquals(mock.getDBVersion(),PasswordDbHelper.TABLE_NAME_STATISTIC);
    }
*/

}
