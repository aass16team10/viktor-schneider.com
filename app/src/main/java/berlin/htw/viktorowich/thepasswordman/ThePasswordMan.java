package berlin.htw.viktorowich.thepasswordman;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;

public class ThePasswordMan extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    private static final String LOG_TAG = ThePasswordMan.class.getSimpleName();
    private  SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_password_man);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            PasswordDbHelper passwordDbHelper = new PasswordDbHelper(getApplicationContext());
            this.db = passwordDbHelper.getReadableDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.the_password_man, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"not implemented",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            Intent intent = new Intent(this, ThePasswordMan.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_graph) {
            Intent intent = new Intent(this, Graph.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_create) {
            Intent intent = new Intent(this, Create.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_download) {
            Intent intent = new Intent(this, Download.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_help) {
            Intent intent = new Intent(this, Help.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getPasswords(View view)
    {
        EditText search = (EditText) findViewById(R.id.password_search);
        if (search != null)
        {

            if(!this.db.isOpen())
            {
                Log.d("Achtung", "DB ist closed");
            }
            else
            {
                Log.d("Achtung", "DB ist open");
            }
            String user_input = search.getText().toString().trim();
            //Log.d(LOG_TAG,"%%%%%%%%%%%% 1. user input OK");
            String sql = "SELECT "+PasswordDbHelper.PASSWORD_TEXT+" , "+PasswordDbHelper.PASSWORD_ID+" FROM "+PasswordDbHelper.TABLE_NAME
                    +" WHERE "+ PasswordDbHelper.PASSWORD_TEXT+" LIKE '%"+user_input+"%' LIMIT 20";
            Cursor cursor = db.rawQuery(sql,null);
            //Log.d(LOG_TAG,"%%%%%%%%%%%% 2. cursor OK ------"+sql);
            CursorAdapter cursorAdaptor = new CursorAdapter(this,cursor,0);
            Log.d(LOG_TAG,"%%%%%%%%%%%% 3. cursorAdapter OK Count:"+cursor.getCount());
            ListView lv = (ListView) findViewById(R.id.passwords_list_view);
            assert lv != null;
            lv.setAdapter(cursorAdaptor);
        }
        //this.db.close();
    }
}
