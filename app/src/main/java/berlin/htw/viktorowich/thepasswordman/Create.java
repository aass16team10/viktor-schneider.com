package berlin.htw.viktorowich.thepasswordman;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

import java.security.SecureRandom;
import java.util.ArrayList;

public class Create extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private CheckBox number, capitals,small_letters,special_character;
    private EditText pw_length;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.number = (CheckBox) findViewById(R.id.checkBox_numbers);
        this.capitals = (CheckBox) findViewById(R.id.checkBox_capitals);
        this.small_letters = (CheckBox) findViewById(R.id.checkBox_small_letters);
        this.special_character = (CheckBox) findViewById(R.id.checkBox_special_character);

        this.pw_length = (EditText) findViewById(R.id.pw_length);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"not implemented",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            Intent intent = new Intent(this, ThePasswordMan.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_graph) {
            Intent intent = new Intent(this, Graph.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_create) {
            Intent intent = new Intent(this, Create.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_download) {
            Intent intent = new Intent(this, Download.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_help) {
            Intent intent = new Intent(this, Help.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void createPassword(View view)
    {
        boolean number =this.number.isChecked();
        boolean capitals =this.capitals.isChecked();
        boolean small_letters=this.small_letters.isChecked();
        boolean special_character = this.special_character.isChecked();

        int pw_length = 8;
        if(!this.pw_length.getText().toString().isEmpty())
        {
            pw_length = Integer.parseInt(this.pw_length.getText().toString());
        }
        StringBuilder random_string = new StringBuilder("");
        final String numbers = "0123456789";
        final String l_capitals = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String l_small = "abcdefghijklmnopqrstuvwxyz";
        final String special = "!\"#$%&'()*+,-./ ;<=>?@\\]^_|}{~";



        if(capitals)
        {
            random_string.append(l_capitals);
        }
        if(small_letters)
        {
            random_string.append(l_small);
        }

        if(number)
        {
            random_string.append(numbers);
        }

        if(special_character)
        {
            random_string.append(special);
        }

        ArrayList<String> pw_liste = new ArrayList<>();

        for ( int a = 0; a <20; a++)
        {
            SecureRandom rnd = new SecureRandom();
            StringBuilder sb = new StringBuilder( pw_length );
            for( int i = 0; i < pw_length; i++ )
            {
                sb.append( random_string.charAt( rnd.nextInt(random_string.length()) ) );
            }
            pw_liste.add(sb.toString());
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, pw_liste);
        ListView lv = (ListView) findViewById(R.id.create_pw_listview);
        assert lv != null;
        lv.setAdapter(itemsAdapter);


    }
}
