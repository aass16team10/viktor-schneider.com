package berlin.htw.viktorowich.thepasswordman;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by viktorowich on 27/05/16.
 */
public class PasswordDbHelper extends SQLiteOpenHelper
{

    private String LOG_TAG = PasswordDbHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "passwords.db";
    private static final int DATABASE_VERSION = 13;

   // private static final String COMMA_SEP = ",";
    public static final String TABLE_NAME = "pw";
    public static final String PASSWORD_ID = "_id";
    public static final String PASSWORD_TEXT = "text";
    public static final String PASSWORD_LENGTH = "length";
    public static final String PASSWORD_CREATED = "created";

    public static final String TABLE_NAME_STATISTIC = "statistic";
    public static final String STATISTIC_CHAR = "statistic_char";
    public static final String STATISTIC_COUNT = "statistic_count";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_NAME + " ( " +
                    PASSWORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PASSWORD_TEXT + " TEXT, " +
                    PASSWORD_LENGTH + " INTEGER, " +
                    PASSWORD_CREATED + " TEXT DEFAULT CURRENT_TIMESTAMP " +
                    " )";
    private static final String TABLE_CREATE_STATISTIC =
            "CREATE TABLE " + TABLE_NAME_STATISTIC + " ( " +
                    STATISTIC_CHAR + " TEXT, " +
                    STATISTIC_COUNT + " INTEGER " +
                    " )";


    private Context context;

    public PasswordDbHelper(Context context) throws IOException
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(LOG_TAG, "DbHelper hat die Datenbank: " + getDatabaseName() + " erzeugt.");
        this.context = context;

    }

    public int getDBVersion()
    {
        return DATABASE_VERSION;
    }
    public String getDBname()
    {
        return DATABASE_NAME;
    }
    public String getDBnameStatistic()
    {
        return TABLE_CREATE_STATISTIC;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_CREATE_STATISTIC);

        BufferedReader reader =
                null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(this.context.getAssets().open("passwords.txt"))
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // this.passwordDbHelper = new PasswordDbHelper(context);
//        db = this.getWritableDatabase();
        String sql = "INSERT INTO "+TABLE_NAME
                + " ("+PASSWORD_TEXT+","+PASSWORD_LENGTH+")"
                +" VALUES (?,?)";
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(sql);

        HashMap<Character,Integer> map = new HashMap<Character,Integer>();

        String mLine = null;

        try {
            assert reader != null;
            mLine = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (mLine != null)
        {
            stmt.bindString(1,mLine);
            stmt.bindLong(2,mLine.length());
            stmt.execute();
            stmt.clearBindings();

            // zählt die chars
            for(int i = 0; i < mLine.length(); i++){
                char c = mLine.charAt(i);
                Integer val = map.get(Character.valueOf(c));
                if(val != null){
                    map.put(c, val + 1);
                }else{
                    map.put(c,1);
                }
            }

            try {
                mLine = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String sql_statistic = "INSERT INTO "+TABLE_NAME_STATISTIC
                + " ("+STATISTIC_CHAR+","+STATISTIC_COUNT+")"
                +" VALUES (?,?)";

        SQLiteStatement stmt_statistic = db.compileStatement(sql_statistic);
        for(Map.Entry<Character,Integer> entry : map.entrySet())
        {
            Character c = entry.getKey();
            int i = entry.getValue();
            stmt_statistic.bindString(1, String.valueOf(c));
            stmt_statistic.bindLong(2,i);
            stmt_statistic.execute();
            stmt_statistic.clearBindings();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        Log.d("$$$$$$$$$$$$$$$$$$$$", Arrays.asList(map).toString());
        Log.d("DB","onCreate wurde aufgerufen");
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_STATISTIC);
        onCreate(db);
        Log.d("Version","die alte:"+oldVersion + "  und die neue : "+newVersion);
    }


}
