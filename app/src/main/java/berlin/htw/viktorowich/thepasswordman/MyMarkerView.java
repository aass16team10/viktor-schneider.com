package berlin.htw.viktorowich.thepasswordman;

import android.content.Context;
import android.widget.TextView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.Utils;

/**
 * Created by viktorowich on 11/07/16.
 */
public class MyMarkerView extends MarkerView
{

    private TextView marked_char_text;
    private BarChart barChart;
    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param layoutResource the layout resource to use for the MarkerView
     */
    public MyMarkerView(Context context, int layoutResource, BarChart barChart)
    {
        super(context, layoutResource);
        marked_char_text = (TextView) findViewById(R.id.marked_char);
        this.barChart = barChart;
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight)
    {
        marked_char_text.setText(
                String.format("\"%s\": %s",
                        barChart.getData().getXVals().get(e.getXIndex()),
                        (int)e.getVal())
        );
    }

    @Override
    public int getXOffset(float xpos) {
        return -(getWidth() / 2);
    }

    @Override
    public int getYOffset(float ypos) {
        return -getHeight();
    }
}
