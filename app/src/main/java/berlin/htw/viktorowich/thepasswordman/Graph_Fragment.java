package berlin.htw.viktorowich.thepasswordman;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.io.IOException;
import java.util.ArrayList;


public class Graph_Fragment extends Fragment
{

    private SQLiteDatabase db;

    public Graph_Fragment() {
        // Required empty public constructor
    }
    private BarChart barChart ;
    BarData barData;




    @Override
    public void onCreate(Bundle savedInstanceState)
    {


        try {
            PasswordDbHelper passwordDbHelper = new PasswordDbHelper(getActivity());
            this.db = passwordDbHelper.getReadableDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!this.db.isOpen())
        {
            Log.d("Achtung", "DB ist closed");
        }
        else
        {
            Log.d("Achtung", "DB ist open");
        }


        String sql = "SELECT "
                +PasswordDbHelper.STATISTIC_CHAR+" , "
                +PasswordDbHelper.STATISTIC_COUNT+" FROM "
                +PasswordDbHelper.TABLE_NAME_STATISTIC;

        Cursor cursor = db.rawQuery(sql,null);
        ArrayList<BarEntry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();


        // if (cursor != null)
        // {
        float i = 0;
        if(cursor.moveToFirst())
        {
             while (cursor.moveToNext())
             {
                 String statistic_char = cursor.getString(cursor.getColumnIndex(PasswordDbHelper.STATISTIC_CHAR));
                 int statistic_count = cursor.getInt(cursor.getColumnIndex(PasswordDbHelper.STATISTIC_COUNT));
                 entries.add(new BarEntry(statistic_count, (int) i));
                 labels.add(statistic_char);
                 i++;
            }
        }
        BarDataSet dataSet = new BarDataSet(entries,"Single Chars");
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        //dataSet.setValueTextSize((float) 6.0);
        dataSet.setDrawValues(false);
        dataSets.add(dataSet);
        this.barData = new BarData(labels,dataSets);

        cursor.close();
        //}

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_graph_, container, false);
        //this.barChart = new BarChart(getActivity());
        this.barChart = (BarChart) view.findViewById(R.id.chart);
        //FrameLayout content = (FrameLayout)view.findViewById(R.id.fragment_content);
        this.barChart.setData(this.barData);
        XAxis xAxis = this.barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        this.barChart.setDescription("Counting Chars in all passwords");
        MyMarkerView myMarkerView = new MyMarkerView(getContext(),R.layout.marker_view,this.barChart);
        this.barChart.setMarkerView(myMarkerView);
        this.barChart.animateY(3000, Easing.EasingOption.Linear);

        this.barChart.getAxisRight().setDrawLabels(false);

        return view;

    }
}
