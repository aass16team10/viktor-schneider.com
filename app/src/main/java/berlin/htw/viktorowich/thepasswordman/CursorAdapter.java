package berlin.htw.viktorowich.thepasswordman;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by viktorowich on 12/06/16.
 */
public class CursorAdapter extends android.widget.CursorAdapter
{

    public CursorAdapter(Context context, Cursor c, int flag)
    {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return LayoutInflater.from(context).inflate(R.layout.thepasswordman_line_view, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView password = (TextView) view.findViewById(R.id.password_line);
        String text = cursor.getString(cursor.getColumnIndexOrThrow("text"));
        password.setText(text);
    }
}
