package berlin.htw.viktorowich.thepasswordman;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Download extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DownloadManager manager = null;
    private SQLiteDatabase db ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    BroadcastReceiver onComplete = new BroadcastReceiver()
    {
        public void onReceive(Context ctxt, Intent intent)
        {
            Toast.makeText(getApplicationContext(),"Download fertig",Toast.LENGTH_SHORT).show();

            try {
                PasswordDbHelper passwordDbHelper = new PasswordDbHelper(getApplicationContext());
                db = passwordDbHelper.getWritableDatabase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader reader =
                    null;
            try {
                reader = new BufferedReader(
                        // TODO: abfragen mit if welches genommen werden soll
                      new FileReader(Environment.DIRECTORY_DOWNLOADS+"/7-more-passwords.txt")
                        //new FileReader("/storage/emulated/0/Download/7-more-passwords.txt")

                );
            } catch (IOException e) {
                e.printStackTrace();
            }
//Environment.DIRECTORY_DOWNLOADS)+"/"+"7-more-passwords.txt"
            // this.passwordDbHelper = new PasswordDbHelper(context);
//        db = this.getWritableDatabase();
            String sql = "INSERT INTO "+PasswordDbHelper.TABLE_NAME
                    + " ("+PasswordDbHelper.PASSWORD_TEXT+","+PasswordDbHelper.PASSWORD_LENGTH+")"
                    +" VALUES (?,?)";
            db.beginTransaction();
            SQLiteStatement stmt = db.compileStatement(sql);

            HashMap<Character,Integer> map = new HashMap<Character,Integer>();

            String mLine = null;

            try {
                assert reader != null;
                mLine = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (mLine != null)
            {
                stmt.bindString(1,mLine);
                stmt.bindLong(2,mLine.length());
                stmt.execute();
                stmt.clearBindings();

                // zählt die chars
                for(int i = 0; i < mLine.length(); i++){
                    char c = mLine.charAt(i);
                    Integer val = map.get(Character.valueOf(c));
                    if(val != null){
                        map.put(c, val + 1);
                    }else{
                        map.put(c,1);
                    }
                }

                try {
                    mLine = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String sql_statistic = "INSERT INTO "+PasswordDbHelper.TABLE_NAME_STATISTIC
                    + " ("+PasswordDbHelper.STATISTIC_CHAR+","+PasswordDbHelper.STATISTIC_COUNT+")"
                    +" VALUES (?,?)";

            SQLiteStatement stmt_statistic = db.compileStatement(sql_statistic);
            for(Map.Entry<Character,Integer> entry : map.entrySet())
            {
                Character c = entry.getKey();
                int i = entry.getValue();
                stmt_statistic.bindString(1, String.valueOf(c));
                stmt_statistic.bindLong(2,i);
                stmt_statistic.execute();
                stmt_statistic.clearBindings();
            }

            db.setTransactionSuccessful();
            db.endTransaction();
            Toast.makeText(getApplicationContext(),"DB wurde erweitert.",Toast.LENGTH_SHORT).show();
        }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"not implemented",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {
            Intent intent = new Intent(this, ThePasswordMan.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_graph) {
            Intent intent = new Intent(this, Graph.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_create) {
            Intent intent = new Intent(this, Create.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_download) {
            Intent intent = new Intent(this, Download.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_help) {
            Intent intent = new Intent(this, Help.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, About.class);
            startActivity(intent);
            return true;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void loadFile(View view)
    {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            Log.v("Erlaubnis: ","Permission is granted");

        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }
        loadFileFunction();

    }
    @TargetApi(Build.VERSION_CODES.M)
    public boolean loadFileFunction()
    {

        File f = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/"+"7-more-passwords.txt");//(Environment.DIRECTORY_DOWNLOADS,"7-more-passwords.txt");
        Log.d("Datei",f.getPath());
        if(f.exists())
        {
            Toast.makeText(this,"Datei wurde bereits heruntergeladen",Toast.LENGTH_SHORT).show();
            Log.d("Datei", "Datei gibt es schon");
            return false;
        }
        else
        {
            Log.d("Datei", "Datei gibt es nicht");
            String url = "http://download.viktor-schneider.com/7-more-passwords.txt";
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription("Password file");
            request.setTitle("Download");
            // in order for this if to run, you must use the android 3.2 to compile your app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }
            // request.setDestinationInExternalFilesDir(Environment.DIRECTORY_DOWNLOADS,null,"7-more-passwords.txt");
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "7-more-passwords.txt");

            // get download service and enqueue file

            this.manager.enqueue(request);
            return true;
        }
    }
}
